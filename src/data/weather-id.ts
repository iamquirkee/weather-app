const WEATHER_ID: {
  [key: number]: {
    name: string,
    desc: string,
    warnUser: boolean,
    icoClass: {
      day: string,
      night: string
    }
  }
} = {
  200: {
    name: "Thunderstorm",
    desc: "Thunderstorm w/Light rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-thunderstorm",
      night: "wi-night-alt-thunderstorm",
    },
  },
  201: {
    name: "Thunderstorm",
    desc: "Thunderstorm w/Rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-thunderstorm",
      night: "wi-night-alt-thunderstorm",
    },
  },
  202: {
    name: "Thunderstorm",
    desc: "Thunderstorm w/Heavy rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-thunderstorm",
      night: "wi-night-alt-thunderstorm",
    },
  },
  210: {
    name: "Thunderstorm",
    desc: "Light Thunderstorm",
    warnUser: true,
    icoClass: {
      day: "wi-day-thunderstorm",
      night: "wi-night-alt-thunderstorm",
    },
  },
  211: {
    name: "Thunderstorm",
    desc: "Thunderstorm",
    warnUser: true,
    icoClass: {
      day: "wi-day-thunderstorm",
      night: "wi-night-alt-thunderstorm",
    },
  },
  212: {
    name: "Thunderstorm",
    desc: "Heavy Thunderstorm",
    warnUser: true,
    icoClass: {
      day: "wi-day-thunderstorm",
      night: "wi-night-alt-thunderstorm",
    },
  },
  221: {
    name: "Thunderstorm",
    desc: "Ragged Thunderstorm",
    warnUser: true,
    icoClass: {
      day: "wi-day-thunderstorm",
      night: "wi-night-alt-thunderstorm",
    },
  },
  230: {
    name: "Thunderstorm",
    desc: "Thunderstorm w/Light drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-storm-showers",
      night: "wi-night-storm-showers",
    },
  },
  231: {
    name: "Thunderstorm",
    desc: "Thunderstorm w/Drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-storm-showers",
      night: "wi-night-storm-showers",
    },
  },
  232: {
    name: "Thunderstorm",
    desc: "Thunderstorm w/Heavy drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-storm-showers",
      night: "wi-night-storm-showers",
    },
  },
  300: {
    name: "Drizzle",
    desc: "Light drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  301: {
    name: "Drizzle",
    desc: "Drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  302: {
    name: "Drizzle",
    desc: "Heavy drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  310: {
    name: "Drizzle",
    desc: "Light drizzle rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  311: {
    name: "Drizzle",
    desc: "Drizzle rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  312: {
    name: "Drizzle",
    desc: "Heavy drizzle rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  313: {
    name: "Drizzle",
    desc: "Shower rain and drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  314: {
    name: "Drizzle",
    desc: "Heavy shower rain and drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  321: {
    name: "Drizzle",
    desc: "Shower drizzle",
    warnUser: true,
    icoClass: {
      day: "wi-day-showers",
      night: "wi-night-alt-showers",
    },
  },
  500: {
    name: "Rain",
    desc: "Light rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  501: {
    name: "Rain",
    desc: "Moderate rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  502: {
    name: "Rain",
    desc: "Heavy rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  503: {
    name: "Rain",
    desc: "Very Heavy rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  504: {
    name: "Rain",
    desc: "Extreme rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  511: {
    name: "Rain",
    desc: "Freezing rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  520: {
    name: "Rain",
    desc: "Light shower rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  521: {
    name: "Rain",
    desc: "Shower rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  522: {
    name: "Rain",
    desc: "Heavy shower rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  531: {
    name: "Rain",
    desc: "Ragged shower rain",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain",
      night: "wi-night-rain",
    },
  },
  600: {
    name: "Snow",
    desc: "Light snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-snow",
      night: "wi-night-alt-snow",
    },
  },
  601: {
    name: "Snow",
    desc: "Snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-snow",
      night: "wi-night-alt-snow",
    },
  },
  602: {
    name: "Snow",
    desc: "Heavy snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-snow",
      night: "wi-night-alt-snow",
    },
  },
  611: {
    name: "Snow",
    desc: "Sleet",
    warnUser: true,
    icoClass: {
      day: "wi-day-snow",
      night: "wi-night-alt-snow",
    },
  },
  612: {
    name: "Snow",
    desc: "Light shower sleet",
    warnUser: true,
    icoClass: {
      day: "wi-day-snow",
      night: "wi-night-alt-snow",
    },
  },
  613: {
    name: "Snow",
    desc: "Shower sleet",
    warnUser: true,
    icoClass: {
      day: "wi-day-snow",
      night: "wi-night-alt-snow",
    },
  },
  615: {
    name: "Snow",
    desc: "Light rain and snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain-mix",
      night: "wi-night-alt-rain-mix",
    },
  },
  616: {
    name: "Snow",
    desc: "Rain and snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain-mix",
      night: "wi-night-alt-rain-mix",
    },
  },
  620: {
    name: "Snow",
    desc: "Light shower snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain-mix",
      night: "wi-night-alt-rain-mix",
    },
  },
  621: {
    name: "Snow",
    desc: "Shower snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain-mix",
      night: "wi-night-alt-rain-mix",
    },
  },
  622: {
    name: "Snow",
    desc: "Heavy shower snow",
    warnUser: true,
    icoClass: {
      day: "wi-day-rain-mix",
      night: "wi-night-alt-rain-mix",
    },
  },

  701: {
    name: "Mist",
    desc: "mist",
    warnUser: false,
    icoClass: {
      day: "wi-fog",
      night: "wi-fog",
    },
  },
  711: {
    name: "Smoke",
    desc: "Smoke",
    warnUser: false,
    icoClass: {
      day: "wi-fog",
      night: "wi-fog",
    },
  },
  721: {
    name: "Haze",
    desc: "Hazy",
    warnUser: false,
    icoClass: {
      day: "wi-fog",
      night: "wi-fog",
    },
  },
  731: {
    name: "Dust",
    desc: "Sandy w/Dust swirls",
    warnUser: false,
    icoClass: {
      day: "wi-fog",
      night: "wi-fog",
    },
  },
  741: {
    name: "Fog",
    desc: "Foggy",
    warnUser: false,
    icoClass: {
      day: "wi-fog",
      night: "wi-fog",
    },
  },
  751: {
    name: "Sand",
    desc: "Sandy",
    warnUser: false,
    icoClass: {
      day: "wi-sandstorm",
      night: "wi-sandstorm",
    },
  },
  761: {
    name: "Dust",
    desc: "Dusty",
    warnUser: false,
    icoClass: {
      day: "wi-smog",
      night: "wi-smog",
    },
  },
  762: {
    name: "Ash",
    desc: "Volcanic Ash",
    warnUser: true,
    icoClass: {
      day: "wi-volcano",
      night: "wi-volcano",
    },
  },
  771: {
    name: "Squall",
    desc: "Squalls",
    warnUser: false,
    icoClass: {
      day: "wi-hurricane",
      night: "wi-hurricane",
    },
  },
  781: {
    name: "Tornado",
    desc: "Tornado",
    warnUser: true,
    icoClass: {
      day: "wi-tornado",
      night: "wi-tornado",
    },
  },
  800: {
    name: "Clear",
    desc: "clear sky",
    warnUser: false,
    icoClass: {
      day: "wi-day-sunny",
      night: "wi-night-clear",
    }
  },
  801: {
    name: "Clouds",
    desc: "Few clouds",
    warnUser: false,
    icoClass: {
      day: "wi-day-cloudy",
      night: "wi-night-partly-cloudy",
    }
  },
  802: {
    name: "Clouds",
    desc: "Scattered clouds",
    warnUser: false,
    icoClass: {
      day: "wi-day-cloudy",
      night: "wi-night-partly-cloudy",
    }
  },
  803: {
    name: "Clouds",
    desc: "Broken clouds",
    warnUser: false,
    icoClass: {
      day: "wi-day-cloudy",
      night: "wi-night-alt-cloudy",
    }
  },
  804: {
    name: "Clouds",
    desc: "Cloudy",
    warnUser: false,
    icoClass: {
      day: "wi-day-cloudy",
      night: "wi-night-alt-cloudy",
    }
  },
}

export default WEATHER_ID;