import Vue from "vue";
import App from "./App.vue";
// import './registerServiceWorker'
import Store from "./store";
// @ts-ignore
import { InlineSvgPlugin } from "vue-inline-svg";

// @ts-ignore
import runtime from "serviceworker-webpack-plugin/lib/runtime";

// Return store with maps
const store = Store(Vue);

if ("serviceWorker" in navigator) {
  runtime.register();
}

Vue.use(InlineSvgPlugin);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  store,
}).$mount("#app");
