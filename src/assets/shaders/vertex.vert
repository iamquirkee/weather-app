uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

attribute vec3 position;

uniform float uVirtualHeight;
uniform float uVirtualWidth;

uniform float uTime;
uniform float uSheer;
uniform float uOffset;
uniform float uIntensity;
uniform float uRipples;
uniform float uSpeed;
uniform float uTest;

// attribute vec2 uv;
// varying vec2 vUv

void main() {


  // float animation = ((uAnimation * 2.) - 1.) * -1.;

  // Apply initial transforms to mesh
  vec4 modelPosition = modelMatrix * vec4(position, 1.0);

  float xSheer = uSheer * (modelPosition.y / uVirtualHeight) * 2.;

  // Offsets
  modelPosition.x += xSheer * uVirtualWidth;
  // modelPosition.x += animation * (uVirtualWidth + (uVirtualWidth * (abs(uSheer) * uOffset)));

  // Undulation
  modelPosition.x +=
    cos(((position.y * uVirtualHeight * .25) * uRipples) + (uTime * uSpeed))
    * -sin(((position.y * uVirtualWidth * .25) * uRipples) + (uTime * uSpeed * .25))
    * uIntensity * uVirtualWidth;

  modelPosition.y +=
    -cos(((position.y * uVirtualHeight * .25) * uRipples) + (uTime * uSpeed))
    * sin(((position.y * uVirtualWidth * .25) * uRipples) + (uTime * uSpeed * .25))
    * uIntensity * uVirtualWidth;

  // Causes wobble to freak out when animation plane
  // modelPosition.x +=
  //   cos((modelPosition.y * uRipples) + (uTime * uSpeed))
  //   * -sin(modelPosition.x * uRipples)
  //   * uIntensity * uVirtualWidth;

  // Apply transforms relative to the camera
  vec4 viewPosition = viewMatrix * modelPosition;

  // Transform coordinates into clip space
  vec4 projectedPosition = projectionMatrix * viewPosition;


  // vUv = uv;
  gl_Position = projectedPosition;

}