import { precacheAndRoute } from 'workbox-precaching';
import { registerRoute } from 'workbox-routing';
import { StaleWhileRevalidate, CacheFirst, NetworkFirst } from 'workbox-strategies';
import { Plugin as CacheableResponsePlugin } from 'workbox-cacheable-response';
import { Plugin as ExpirationPlugin } from 'workbox-expiration';

const VERSION = '0.1.1';
console.log('[ Service Worker ] Registered', VERSION);

precacheAndRoute(self.serviceWorkerOption.assets);

self.addEventListener('install', (event) => {

  // Force new sw to take over now rather than on refresh
  self.skipWaiting();

  // TODO:
  // Should clear the precache too not just the cache
  // If not WB will freak out till the 2nd install
  event.waitUntil(
    caches.keys().then(cacheNames => {
      cacheNames.forEach(cacheName => {
        caches.delete(cacheName);
      })
    })
  )

})

self.addEventListener('push', function (event) {
  const title = `Weather!`;
  const options = {
    body: event.data.text(),
    icon: "/img/icons/apple-touch-icon-180x180.png",
    badge: "/img/icons/apple-touch-icon-60x60.png",
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

// Image Cache
registerRoute(
  /\.(?:png|gif|jpg|jpeg|svg)$/,
  new StaleWhileRevalidate({
    cacheName: 'images',
    plugins: [
      new ExpirationPlugin({
        maxEntries: 60,
        maxAgeSeconds: 14 * 24 * 60 * 60, // 14 Days
      }),
    ],
  })
);

// Stylesheet Cache
registerRoute(
  ({ url }) => url.origin === 'https://fonts.googleapis.com',
  new StaleWhileRevalidate({
    cacheName: 'google-fonts-stylesheets',
  })
);

// Font Cache
registerRoute(
  ({ url }) => url.origin === 'https://fonts.gstatic.com',
  new CacheFirst({
    cacheName: 'google-fonts-webfonts',
    plugins: [
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }),
      new ExpirationPlugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30,
      }),
    ],
  })
);

// Weather Cache
registerRoute(
  ({ url }) => url.origin === 'https://api.openweathermap.org',
  new NetworkFirst({
    cacheName: 'weather-api',
    plugins: [
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }),
      new ExpirationPlugin({
        maxAgeSeconds: 60 * 60 * 24 * 7,
        maxEntries: 5,
      }),
    ],
  })
);

// Location Cache
registerRoute(
  ({ url }) => url.origin === 'https://api.opencagedata.com',
  new NetworkFirst({
    cacheName: 'location-api',
    plugins: [
      new CacheableResponsePlugin({
        statuses: [0, 200],
      }),
      new ExpirationPlugin({
        maxAgeSeconds: 60 * 60 * 24 * 7,
        maxEntries: 5,
      }),
    ],
  })
);
