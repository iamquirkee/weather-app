import Vuex from 'vuex';

// NOTE:
// Don't think we need to split the code for now

export type PanelTypes = "init" | "onboarding" | "current" | "favourites" | "settings";
export type PositionType = { lat: number, lon: number };
export type CountryType = { name: string, code: string, pos: { lat: number, lon: number } };
export type WeatherUnitTypes = "imperial" | "metric";

interface StoreState {
  isApp: boolean,
  panel: PanelTypes,
  permissions: {
    location: boolean,
    notifications: boolean,
  },
  countries: string[],
  isDeletingCountries: boolean,
  data: {
    current: unknown,
    position: PositionType,
    weather: {
      current: unknown | null,
      hourly: unknown | null,
      daily: unknown | null
    },
  }
}

// TODO:
// Fix the type and enable module type check
const store = (Vue: any): any => {

  Vue.use(Vuex);

  return new Vuex.Store({
    state: {
      isApp: false,
      panel: 'init',
      permissions: {
        location: false,
        notifications: false,
      },
      countries: [],
      isDeletingCountries: false,
      data: {
        current: {},
        position: {
          lat: -1,
          lon: -1,
        },
        weather: {
          current: null,
          hourly: null,
          daily: null
        }
      }
    } as StoreState,
    getters: {
      listCountries: (state: any): string[] => {
        return state.countries;
      },
      isDeletingCountries: (state: any): boolean => {
        return state.isDeletingCountries;
      },
      currentState: (state: any): string => {
        return state.panel;
      },
      currentPosition: (state: any): PositionType => {
        return state.data.position;
      },
      currentLocationData: (state: any): any => {
        return state.data.current;
      },
      currentWeatherData: (state: any): any => {
        return state.data.weather;
      }
    },
    mutations: {
      updatePanelState: (state: any, panelState: PanelTypes) => {
        state.panel = panelState
      },
      addCountryToList: (state: any, country: CountryType) => {
        let _hasCountry = false;
        for (let c = 0; c < state.countries.length; c++) {
          const _countryName = state.countries[c].name;
          if (_countryName === country.name) {
            _hasCountry = true;
          }
        }
        if (!_hasCountry) {
          state.countries = [...state.countries, country];
        }
      },
      removeCountryFromList: (state: any, country: CountryType) => {
        const _countries = [...state.countries];
        const _filtered = _countries.filter((c: CountryType) => {
          return c !== country
        })
        state.countries = [..._filtered];
      },
      updateDeletingCountryState: (state: any, isDeleting: boolean) => {
        state.isDeletingCountries = isDeleting;
      },
      updateCountriesList: (state: any, countries: any[]) => {
        state.countries = [...countries];
      },
      updatePosition: (state: any, position: PositionType) => {
        state.data.position = position;
      },
      updateCurrentLocationData: (state: any, data: any) => {
        state.data.current = data;
      },
      updateCurrentWeatherData: (state: any, data: any) => {
        state.data.weather = data;
      }
    },
    actions: {
      setState: (context: any, state: PanelTypes) => {
        context.commit('updatePanelState', state)
      },
      addCountry: (context: any, country: CountryType) => {
        context.commit('addCountryToList', country)
      },
      removeCoutry: (context: any, country: CountryType) => {
        context.commit('removeCountryFromList', country)
      },
      setDeletingCountryState: (context: any, isDeleting: boolean) => {
        context.commit('updateDeletingCountryState', isDeleting);
      },
      setCountries: (context: any, countries: any[]) => {
        context.commit('updateCountriesList', countries)
      },
      setPosition: (context: any, position: PositionType) => {
        context.commit('updatePosition', position);
      },
      setCurrentLocationData: (context: any, data: any) => {
        context.commit('updateCurrentLocationData', data);
      },
      setCurrentWeatherData: (context: any, data: any) => {
        context.commit('updateCurrentWeatherData', data);
      }
    }
  })

}

export default store;