# weather

## Tested on
- NPM 7.5.3
- Node 15.10.0

## Project setup
```
yarn install
yarn serve
```

## PWA Features
- Workbox to handle cache
- Push notifications via subscription supported by Service worker
- Push notifications for when weather changes and user should be warned
- Offline mode with local storage for caching

## Extra features
- Change units for temp
- Togglable weather condition warnings
- Canvas background for current weather for main display
- Nav animations for toggling between slides

## TODO
- Add support for alerts for current weather data calls to main display
- Canvas background to react based on weather

## Stuff I used
- Frameworks / Packages
  - Vue
  - Axios
  - Debounce
  - Workbox
  - dayjs

- Languages
  - Typescript
  - Pug
  - Sass

- APIS
  - Openweathermap
  - OpenCageData
